<?php include "./code.php"; ?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>

    <div class="container mt-5">
        <div class="card">
            <!-- <img src="..." class="card-img-top" alt="..."> -->
            <div class="card-body">
                <h1 class="card-title">Full Addess</h1>
                <p class="card-text"><?php echo getFullAdress("Purok #7 Mainit san isidro", "Bislig city", "Surigao del sur", "Philiphines") ?></p>

            </div>
        </div>

        <div>
            <h2>Letter-Based Grading</h2>

            <p> <?php echo getLetterGrade(95) ?></p>

            <p> <?php echo getLetterGrade(89) ?></p>

            <p> <?php echo getLetterGrade(80) ?></p>

            <p> <?php echo getLetterGrade(74) ?></p>

        </div>

    </div>
    </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>